# i3lock-color

The world's most popular non-default computer lockscreen.

https://github.com/Raymo111/i3lock-color

<br>

How to clone this repository:
```
git clone https://gitlab.com/rebornos-team/rebornos-packages/i3-packages/i3lock-color.git
```
